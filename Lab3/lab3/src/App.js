import logo from './logo.svg';
import './App.css';
import Header from './components/Header';
import Footer from './components/Footer';
import Body from './components/Body';
import First from './components/First';

function App() {
  return (
    <div className="App">
      <First />
      <Header />
      <Body />
      <Footer />
      <a className="App-link" href="https://www.thairath.co.th/lifestyle/life/1881849" target="_blank" rel="noopener noreferrer">ไปยังเว็ปอ้างอิง--></a>
    </div>
  );
}

export default App;
