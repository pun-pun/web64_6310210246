const express = require('express')
const res = require('express/lib/response')
const { json } = require('express/lib/response')
const app = express()
const port = 4000

app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.post('/bmi', (req, res) => {
    let weight = parseFloat(req.query.weight)
    let height = parseFloat(req.query.height)
    let result = {}

    if( !isNaN(weight) && !isNaN(height)) {
        let bmi = weight / (height * height)

        result = {
            "status" : 200,
            "bmi" : bmi
        }
        
    }else {
        result = {
            "status" : 400,
            "message" : "weight or height is not number"
        }
    }

    res.send(JSON.stringify(result))
})

app.get('/triangle', (req, res) => {
    let high = parseFloat(req.query.high)
    let base = parseFloat(req.query.base)
    let answer = {}

    if( !isNaN(high) && !isNaN(base)) {
        let area = ( high * base ) / 2

        answer = {
            "status" : 200,
            "triangle area" : area
        }
        
    }else {
        answer = {
            "status" : 400,
            "message" : "high or base is not number"
        }
    }

    res.send(JSON.stringify(answer))
})

app.post('/score', (req, res) => {
    let score = parseFloat(req.query.score)
    let name = req.query.name
    let total = {}

    if( !isNaN(score)) {
        let grade = {}

        if( score >= 80) {
            grade = 'A'
        }
        else if( score >= 65 ) {
            grade = 'B'
        }
        else if( score >= 50 ) {
            grade = 'C'
        }
        else if( score >= 35) {
            grade = 'D'
        }
        else {
            grade = 'E'
        }

        total = {
            "status" : 200,
            "name" : name,
            "grade" : grade
        }
        
    }else {
        total = {
            "status" : 400,
            "message" : "score is not number"
        }
    }

    res.send(JSON.stringify(total))
})

app.get('/hello', (req, res) => {
    res.send('Sawasdee ' + req.query.name)
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})