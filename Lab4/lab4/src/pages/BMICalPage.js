import { useState } from "react";
import BMIResult from "../components/BMIResult";

function BMICalPage() {

    const [ name, setName ] = useState();
    const [ bmiResult, setBmiResult ] = useState(0);
    const [ translateResult, setTranslateResult ] = useState("");

    const [ height, setHeight ] = useState("");
    const [ weight, setweight ] = useState("");
    
    function calculateBMI() {
        let h = parseFloat(height);
        let w = parseFloat(weight);
        let bmi = w / (h * h);
        setBmiResult(bmi);
        if (bmi > 25) {
            setTranslateResult("อ้วน");
        }
        else {
            setTranslateResult("ผอม");
        }
    }

    return (
        <div align="left">
            <div align="center">
                ยินดีต้อนรับสู่เว็ปคำนวณ BMI
                <hr />

                คุณชื่อ: <input type="text"
                              value={ name } 
                              onChange={ (e) => {setName(e.target.value);} }/> <br />
                              
                สูง: <input type="text"
                              value={ height } 
                              onChange={ (e) => {setHeight(e.target.value);} }/> <br />
                
                หนัก: <input type="text"
                              value={ weight } 
                              onChange={ (e) => {setweight(e.target.value);} }/> <br />
                <button onClick={ () => { calculateBMI() }}>calculate</button>
                { bmiResult != 0 &&
                    <div>
                        <hr />
                        นี่คือผลการคำนวณ

                        <BMIResult 
                        name = { name } 
                        bmi = { bmiResult }  
                        result = { translateResult }             
                        />
                    </div>
                }
            </div>

        </div>
    );
}

export default BMICalPage;