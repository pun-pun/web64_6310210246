import { useState } from "react";
import LuckyNumber from "../components/LuckyNumber";

function LuckyNumberPage() {

    const [ randommath, setRandomMath ] = useState("-");
    const [ textmath, setTextmath ] = useState();
    const [ translaterandom, setTranslateRandom ] = useState("");

    function RandomNumber() {
        let randomnumber = parseInt(Math.random()*100);
        let textmath = parseInt();

        setRandomMath(randomnumber);
        if (textmath == randomnumber) {
            setTranslateRandom("ถูกแล้วจ้า");
        }
        else {
            setTranslateRandom("ผิด !!");
        }
    }

    return(
        <div align="left">
            
            กรุณาทายตัวเลขที่ต้องการ ระหว่าง 0-99 : <input type="text"
                        value={ textmath } 
                        onChange={ (e) => {setTextmath(e.target.value);} }/> <br />

            <button onClick={ () => { RandomNumber() }}>ทาย</button>

            <hr />

            { textmath != 0 &&
                    <div>
                        <hr />
                        <LuckyNumber
                        random = { translaterandom }
                        Rtext = { randommath }
        
                        />
                    </div>
                }

        </div>
    );

}

export default LuckyNumberPage;