import AboutUs from "../components/AboutUs";

function AboutUsPage() {
    return (
        <div>
            <div align="center">
                <h2> คณะผู้จัดทำ </h2>
                <AboutUs name="ปัณณทัต"
                           address="นี่"
                           province="โน่น"/>
                
                <AboutUs name="ปัณณทัต 2"
                           address="นั่น"
                           province="นู่น"/>
            </div>
        </div>
    );
}

export default AboutUsPage;