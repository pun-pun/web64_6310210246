import { useState } from "react";
import BMIResult from "../components/BMIResult";

import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';
import { Typography, Box, Container } from '@mui/material';


function BMICalPage() {



    
    const [ name, setName ] = useState();
    const [ bmiResult, setBmiResult ] = useState(0);
    const [ translateResult, setTranslateResult ] = useState("");

    const [ height, setHeight ] = useState("");
    const [ weight, setweight ] = useState("");
    
    function calculateBMI() {
        let h = parseFloat(height);
        let w = parseFloat(weight);
        let bmi = w / (h * h);
        setBmiResult(bmi);
        if (bmi > 25) {
            setTranslateResult("อ้วน");
        }
        else {
            setTranslateResult("ผอม");
        }
    }
    return (
        <Container maxWidth='ig'>
        <Grid container spacing={2} sx={{ marginTop : "10px" }}>
            <Grid item xs={12}>
                <Typography variant="h5">
                    ยินดีต้อนรับสู่เว็ปคำนวณ BMI
                </Typography>
            </Grid>
            <Grid item xs={8}>
                <Box sx={{ textAlign : 'left'}}>
                    คุณชื่อ: <input type="text"
                                value={ name } 
                                onChange={ (e) => {setName(e.target.value);} }/> 
                                <br /><br />
            
                    สูง: <input type="text"
                                value={ height } 
                                onChange={ (e) => {setHeight(e.target.value);} }/> 
                                <br /><br />
                
                    หนัก: <input type="text"
                                value={ weight } 
                                onChange={ (e) => {setweight(e.target.value);} }/>                                 <br />
                                <br /><br />
                    
                    <Button variant="contained" onClick ={()=>{ calculateBMI()}}>calculate</Button>
                </Box>
            </Grid>
            <Grid item xs={4}>
                { bmiResult != 0 &&
                    <div>
                        <hr />
                        นี่คือผลการคำนวณ

                        <BMIResult 
                        name = { name } 
                        bmi = { bmiResult }  
                        result = { translateResult }             
                        />
                    </div>
                }
            </Grid>
        </Grid>
        </Container>
    );

/*
    
        <div align="left">
            <div align="center">
                ยินดีต้อนรับสู่เว็ปคำนวณ BMI
                <hr />

                คุณชื่อ: <input type="text"
                              value={ name } 
                              onChange={ (e) => {setName(e.target.value);} }/> <br />
                              
                สูง: <input type="text"
                              value={ height } 
                              onChange={ (e) => {setHeight(e.target.value);} }/> <br />
                
                หนัก: <input type="text"
                              value={ weight } 
                              onChange={ (e) => {setweight(e.target.value);} }/> <br />
                <Button variant="contained">calculate</Button>
                { bmiResult != 0 &&
                    <div>
                        <hr />
                        นี่คือผลการคำนวณ

                        <BMIResult 
                        name = { name } 
                        bmi = { bmiResult }  
                        result = { translateResult }             
                        />
                    </div>
                }
            </div>

        </div>
    ); */
}

export default BMICalPage;